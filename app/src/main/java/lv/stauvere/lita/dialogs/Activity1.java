package lv.stauvere.lita.dialogs;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;
import android.content.Intent;

public class Activity1 extends AppCompatActivity {

    public static final String TAG = "inlineDebug";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_1);
    }

    public void clickDialog(View view){
        Log.d(TAG, "Tiek izsaukts Dialoga logs");
        MultiSelection demo = new MultiSelection();
        demo.show(getSupportFragmentManager(),"multi_demo");
        //Toast.makeText(this, "Atver Dialog", Toast.LENGTH_LONG).show();
    }

    public void clickActivity2(View view) {
        Log.d(TAG, "Tiek izsaukta Aktivitate2");
        //Toast.makeText(this, "Atver Activity2", Toast.LENGTH_LONG).show();
        Intent intent = new Intent(Activity1.this, Activity2.class);
        startActivity(intent);
    }

}
