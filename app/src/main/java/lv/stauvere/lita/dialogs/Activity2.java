package lv.stauvere.lita.dialogs;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;
import android.content.Intent;

public class Activity2 extends AppCompatActivity {

    public static final String TAG = "inlineDebug";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_2);
    }

    public void clickActivity1(View v) {
        Log.d(TAG, "Tiek izsaukta Aktivitate1");
        //Toast.makeText(this, "Atver Activity1", Toast.LENGTH_LONG).show();
        Intent intent = new Intent(Activity2.this, Activity1.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
    }

}
