package lv.stauvere.lita.dialogs;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.widget.Toast;

import java.util.ArrayList;

public class MultiSelection extends DialogFragment {

    public static final String TAG = "inlineDebug";

    ArrayList<String> list = new ArrayList<String>();

    @Override
    @NonNull
    public Dialog onCreateDialog(Bundle savedInstanceState){
        final String[] items = getResources().getStringArray(R.array.grupas_biedri);

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle(R.string.dialog_title).setMultiChoiceItems(R.array.grupas_biedri, null, new DialogInterface.OnMultiChoiceClickListener() {

            @Override
            public void onClick(DialogInterface dialogInterface, int which, boolean isChecked) {
                
                for (String ms : list){
                    if (isChecked){
                        list.add(items[which]);
                    } else list.remove(items[which]);
                }
                if (isChecked) {
                    Toast.makeText(getActivity(), (items[which] + " izvēlēts!"), Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(getActivity(), (items[which] + " NEizvēlēts!"), Toast.LENGTH_SHORT).show();
                }
            }
        }).setPositiveButton(R.string.ok_label, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int which) {
                Toast.makeText(getActivity(), "Labi poga..", Toast.LENGTH_SHORT).show();
            }
        }).setNegativeButton(R.string.dismiss_label, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
                Toast.makeText(getActivity(), "Atcelt poga..", Toast.LENGTH_SHORT).show();
            }
        });
        return builder.create();
    }
}